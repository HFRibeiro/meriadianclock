#!/usr/bin/env python
from fbs_runtime.application_context.PyQt5 import ApplicationContext
from PyQt5.QtCore import QDateTime, Qt, QTimer
from PyQt5 import QtCore, QtWebSockets,  QtNetwork
from PyQt5.QtWidgets import (QApplication, QCheckBox, QComboBox, QDateTimeEdit,
                             QDial, QDialog, QGridLayout, QGroupBox, QHBoxLayout, QLabel, QLineEdit,
                             QProgressBar, QPushButton, QRadioButton, QScrollBar, QSizePolicy,
                             QSlider, QSpinBox, QStyleFactory, QTableWidget, QTabWidget, QTextEdit,
                             QVBoxLayout, QWidget)

import sys

class MyServer(QtCore.QObject):
    def __init__(self, parent):
        super(QtCore.QObject, self).__init__(parent)
        self.clients = []
        self.server = QtWebSockets.QWebSocketServer(parent.serverName(), parent.secureMode(), parent)
        if self.server.listen(QtNetwork.QHostAddress.Any, 1234):
            print('Connected: '+self.server.serverName()+' : '+self.server.serverAddress().toString()+':'+str(self.server.serverPort()))
        else:
            print('error')
        self.server.newConnection.connect(self.onNewConnection)

        print(self.server.isListening())

    def onNewConnection(self):
        self.clientConnection = self.server.nextPendingConnection()
        self.clientConnection.textMessageReceived.connect(self.processTextMessage)

        self.clientConnection.binaryMessageReceived.connect(self.processBinaryMessage)
        self.clientConnection.disconnected.connect(self.socketDisconnected)

        self.clients.append(self.clientConnection)

    def processTextMessage(self,  message):
        if (self.clientConnection):
            print(message)
            self.clientConnection.sendTextMessage(message)

    def processBinaryMessage(self,  message):
        if (self.clientConnection):
            self.clientConnection.sendBinaryMessage(message)

    def socketDisconnected(self):
        if (self.clientConnection):
            self.clients.remove(self.clientConnection)
            self.clientConnection.deleteLater()


class WidgetGallery(QDialog):
    def __init__(self, parent=None):
        super(WidgetGallery, self).__init__(parent)

        self.originalPalette = QApplication.palette()
        QApplication.setPalette(QApplication.style().standardPalette())

        self.createTopGroupBox()
        self.createBottomGroupBox()

        topLayout = QVBoxLayout()

        mainLayout = QGridLayout()
        topLayout.addWidget(self.topGroupBox, 1)
        topLayout.addWidget(self.bottomGroupBox, 1)
        mainLayout.addLayout(topLayout, 0, 0, 0, 0)
        self.setLayout(mainLayout)

        self.setWindowTitle("Styles")
        QApplication.setStyle(QStyleFactory.create('Fusion'))

    def createTopGroupBox(self):
        self.topGroupBox = QGroupBox("Group Top")
        label = QLabel('Top Label')
        layout = QVBoxLayout()
        layout.addWidget(label)
        layout.addStretch(1)
        self.topGroupBox.setLayout(layout)

    def createBottomGroupBox(self):
        self.bottomGroupBox = QGroupBox("Group Bottom")
        label = QLabel('Bottom Label')
        layout = QVBoxLayout()
        layout.addWidget(label)
        layout.addStretch(1)
        self.bottomGroupBox.setLayout(layout)


if __name__ == '__main__':
    appctxt = ApplicationContext()
    gallery = WidgetGallery()
    gallery.showFullScreen()
    serverObject = QtWebSockets.QWebSocketServer('My Socket', QtWebSockets.QWebSocketServer.NonSecureMode)
    server = MyServer(serverObject)
    serverObject.closed.connect(sys.exit(appctxt.app.exec_()))
